# Question-1

- What is the Feynman Technique? Explain in 1 line.
  - Feyman Technique is a technique used to enchance the learning skills by following a simple rule, **If you can't explain it you can't understand it!**

# Question-2

- In this video, what was the most interesting story or idea for you?
  - In video I like the part of **How do you change your brain?**, in this concept I got know that the way we learn is the key to explain like as previously mentioned the **Feyman Technique** ,I got came up like if you are able to teach to the others in an effective way so that even a child can understand what you are saying, then that's the point where even complex things sorts into a small topic.

# Question-3

- What are active and diffused modes of thinking?
  - The active mode of thinking involves focused and concentrated cognitive efforts, while the diffused mode is a more relaxed state of thinking that allows for subconscious processing and creative insights.

# Question-4

- According to the video, what are the steps to take when approaching a new topic? Only mention the points.
  - Deconstruct the skill
  - Make a graph of daily performance
  - Make 20 hours work plan
  - Have fun in those 20 hours.
  - Remove all the barriers and be free.

# Question-5

- What are some of the actions you can take going forward to improve your learning process?

  1. **Set Clear Goals:** Define specific learning objectives to stay focused and motivated.

  2. **Create a Study Plan:** Organize your time effectively with a structured study schedule.

  3. **Use Active Learning Techniques:** Engage with the material actively through practices like summarizing, teaching, or problem-solving.

  4. **Take Breaks:** Allow time for rest and relaxation to enhance concentration and prevent burnout.

  5. **Seek Understanding, Not Memorization:** Aim to comprehend concepts instead of rote memorization for deeper learning.

  6. **Diversify Learning Methods:** Combine various resources, such as textbooks, videos, and interactive tools, to cater to different learning styles.

  7. **Practice Retrieval:** Test your knowledge regularly through quizzes or recalling information to reinforce memory.
