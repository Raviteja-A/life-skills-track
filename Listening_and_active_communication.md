# Listening and Active Communication

## Question-1

- What are the steps/strategies to do Active Listening? (Minimum 6 points)
  1. **Give full attention:** Eliminate distractions and show the speaker that you are fully engaged in the conversation.
  2. **Show that you are listening:** Use non-verbal cues such as nodding, smiling, and maintaining an open and inviting posture. Provide verbal affirmations, such as "I see," "I understand," or "Go on."
  3. **Paraphrase and summarize:** Repeat back what you've heard in your own words to confirm understanding.
  4. **Ask clarifying questions:** Seek clarification when something is unclear or ambiguous.
  5. **Provide feedback:** Offer feedback that shows you are engaged and interested, such as "That makes sense," or "I appreciate you sharing that."
  6. Avoid interrupting and allow the speaker to express themselves fully before responding.

## Question-2

- According to Fisher's model, what are the key points of Reflective Listening?
  1. _Repeat or Paraphrase:_ Say back what the other person has said using your own words. This shows them that you're paying attention and trying to understand.
  2. _Ask Open-ended Questions:_ Encourage the speaker to share more by asking questions that require more than a simple "yes" or "no" answer. This helps deepen the conversation.
  3. _Clarify Feelings:_ Try to understand the emotions behind the words. Reflect on and acknowledge the speaker's feelings to show empathy and understanding.
  4. _Summarize:_ Recap the main points of what the other person has said. This not only reinforces your understanding but also lets them know you've been actively listening.
  5. _Validate:_ Acknowledge the speaker's perspective, even if you don't necessarily agree. This helps in creating an environment of respect and openness.

## Question-3

- What are the obstacles in your listening process?
  1. Things like noise or phones can take your attention away from what someone is saying.
  2. Judging or having fixed ideas about the person or topic can make you close-minded.
  3. Assuming what someone will say or deciding too quickly what their message is about might lead to misunderstandings.
  4. Only paying attention to parts of the message and ignoring the rest can cause you to miss important information.
  5. I'm alway's worried about my issues so that will result in lack of intreset when some one says something.

# Question 4

- What can you do to improve your listening?
  1. Try to be fully in the moment, clear your mind, and pay attention to the person talking.
  2. Be aware of any judgments you might have and try to put them aside to listen openly.
  3. Instead of assuming, ask questions to make sure you really understand what the person is saying.
  4. Let the person finish what they're saying before you respond. Interrupting can cause misunderstandings.
  5. Try to understand how the person feels. Acknowledge their emotions to connect better.

# Question-5

- When do you switch to Passive communication style in your day to day life?
  - Passive communication is like being too quiet or not saying what you really think
    or feel. People might switch to passive communication in their day-to-day life when: 1. When there is an argue araises then they don't want to argue or disagree, so they stay quiet even if they have a different opinion. 2. They worry that if they express themselves, others might not like them, so they keep their thoughts to themselves. 3. When someone feels unsure or not confident, they may choose to stay quiet instead of speaking up. 4. If someone is afraid of being judged or criticized, they might choose not to share their thoughts or feelings.

# Question-6

- When do you switch into Aggressive communication styles in your day to day life?
  - Switching to aggressive communication means being too forceful or demanding. People might switch to an aggressive communication style in their daily life when:
    1. When someone is upset or frustrated, they might become aggressive to vent their feelings.
    2. Some people use aggression to try to control situations or get their way.
    3. Aggressive communicators often focus on their own needs and opinions without considering others.
    4. Stressful situations may trigger aggressive behavior as a way of coping with pressure.

# Question-7

- When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
  1. Instead of openly expressing their feelings or concerns, someone might use passive-aggressive tactics to avoid a direct confrontation.
  2. When upset, rather than stating their dissatisfaction openly, individuals may use sarcasm or make indirect comments to convey their displeasure.
  3. Passive-aggressive communication can be a way of punishing or getting back at someone without explicitly stating the issue.
  4. Passive-aggressive behaviors can create tension or discomfort in a situation without directly addressing the underlying problems.

# Question-8

- How can you make your communication assertive?
  1. Regularly reflect on your communication style. Identify areas for improvement and work on building assertiveness gradually.
  2. Assertive communication involves finding solutions that work for everyone involved. Be open to compromise and collaborative problem-solving.
  3. Maintain a calm and composed tone of voice, even when discussing challenging topics. Avoid raising your voice or using hostile language.
  4. Actively listen to others, showing that you value their perspective. Respond thoughtfully to what they say, promoting a constructive exchange of ideas.
  5. Frame your statements using "I" to take ownership of your feelings and opinions. For example, say "I feel" instead of "You always."
