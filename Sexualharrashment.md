# Understanding Sexual Harassment

## Kinds of behavior causing sexual harassment:

1. **Verbal Harassment:**

   - _Sexual Comments:_ Inappropriate remarks about a person's body, clothing, or appearance.
   - _Sexual Jokes:_ Humor of a sexual nature that may be offensive or uncomfortable for others.

2. **Non-Verbal Harassment:**

   - _Unwanted Touching:_ Any form of physical contact without explicit consent.
   - _Staring or Leering:_ Prolonged and unwanted gazes with sexual undertones.

3. **Visual Harassment:**

   - _Display of Explicit Material:_ Sharing or exposing sexually suggestive images or content without consent.
   - _Inappropriate Gestures:_ Non-verbal actions with sexual connotations, contributing to discomfort.

4. **Online Harassment:**

   - _Cyberbullying:_ Sending explicit or suggestive messages through digital platforms.
   - _Non-consensual Sharing:_ Distributing intimate images or videos without the individual's permission.

5. **Power Dynamics:**

   - _Abuse of Authority:_ Exploiting positions of power for sexual favors or creating a hostile environment based on hierarchy.

6. **Hostile Environment:**

   - _Sexually Charged Atmosphere:_ Establishing a workplace culture where sexual comments, jokes, or behaviors are pervasive.

7. **Retaliation:**

   - _Reprisals for Reporting:_ Punishing or mistreating individuals who report harassment, creating a culture of fear.

8. **Discrimination:**

   - _Sexual Orientation Discrimination:_ Harassing someone based on their sexual orientation.
   - _Gender-Based Discrimination:_ Targeting individuals due to their gender identity or expression.

9. **Isolation or Exclusion:**

   - _Social Alienation:_ Excluding individuals from workplace activities or conversations based on their response to harassment.

10. **Gaslighting:**
    - _Psychological Manipulation:_ Employing tactics to make the victim doubt their perception of the harassment, minimizing the severity of the behavior.

## What to do if you face or witness any incident:

1. **Immediate Response:**

   - _Assertive Communication:_ Clearly express your discomfort and firmly request the inappropriate conduct to cease.
   - _Safety Measures:_ If applicable, physically remove yourself from the situation to ensure personal safety.

2. **Documentation:**

   - _Detailed Record Keeping:_ Document the incident with specifics like date, time, location, individuals involved, and a thorough description.
   - _Gathering Evidence:_ Preserve any relevant texts, emails, or other records as evidence of the harassment.

3. **Reporting:**

   - _Adherence to Procedures:_ Follow the established company policies for reporting harassment incidents.
   - _Contacting Authorities:_ Reach out to Human Resources, a supervisor, or any designated authority responsible for addressing such matters.

4. **Support Networks:**

   - _Trusted Confidantes:_ Share your experience with a trusted colleague, friend, or family member for emotional support.
   - _Utilizing Counseling Services:_ Take advantage of any counseling services provided by the workplace to navigate the emotional toll.

5. **Legal Protections:**

   - _Know Your Rights:_ Familiarize yourself with local laws and workplace policies concerning sexual harassment.
   - _Consulting Legal Advice:_ If necessary, seek legal counsel to explore potential actions to protect your rights.

6. **Advocacy for Change:**

   - _Participate in Training Programs:_ Engage in workplace training sessions that promote a respectful and inclusive culture.
   - _Promoting Awareness:_ Actively contribute to creating an environment where reporting is encouraged, and harassment is actively discouraged.

7. **Community Involvement:**

   - _Industry Associations:_ Seek support from relevant industry organizations or associations that may provide additional resources.
   - _Advocacy Groups:_ Connect with external groups focused on combating sexual harassment for guidance and support.

8. **Educational Initiatives:**

   - _Workshops and Seminars:_ Advocate for educational initiatives that address the root causes of harassment and promote a culture of respect.
   - _Employee Training:_ Ensure ongoing education for employees about preventing and addressing sexual harassment.

9. **Whistleblower Protections:**

   - _Understanding Protections:_ Be aware of whistleblower protections that may shield you from retaliation for reporting harassment.
   - _Reporting to External Agencies:_ If internal reporting mechanisms fail, explore options to report to external agencies or regulatory bodies.

10. **Policy Review and Enhancement:**
    - _Regular Policy Assessments:_ Advocate for periodic reviews of workplace policies to ensure they remain effective in preventing and addressing sexual harassment.
    - _Feedback Mechanisms:_ Encourage open discussions on policies, seeking feedback from employees for continuous improvement.

Addressing sexual harassment requires a multifaceted approach involving individual actions, organizational commitment, and broader societal awareness. By understanding the nuances of harassment and implementing proactive measures, workplaces can foster environments that are truly respectful, inclusive, and free from harassment.
