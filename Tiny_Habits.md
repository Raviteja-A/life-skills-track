# Tiny Habits

1. Question-1:
   1. As per the video the most intresting Idea for me having a particular plan for daily, it's not for being like a dumb guy, it's just a reminder shedule to make ourself think that what we have to do.
   2. And the second thing is to have an happy moment like "yeah I Did It!", it just boost up our confidence.
2. Question-2
   1. MAP is nothing but motivation, ability,prompt.
   2. MAP is nothing but the way we perform our activity based upon our motivational levels, so inorder to do hard task we need high level of motivation and vice versa, this say's that to prompt a small task instead of doing it by forcing your self .
   3. In my point of view mainting a habit is bit difficult so just have a small part of the habit and move to harder level, it will your confidence like I can do it though like that.
3. Question-3
   1. it's important to celebrate each and every small sucess gives us long time confidence.
   2. Sometimes ourself needs appreciation by us like it's cheering ourselfs by us every day boots our self confidence.
   3. In this part we the one who can disgrace us, so by celebrating our little succes gives us a bit more confidence every day.
4. Question-4
   - The most intresting Idea for me is to be better day by day. Don't keep any higher pressure on us just have a small confidence that I can be better day by day. If we have that much confidence, then by doing better by every day we can master the habit that we are aimed at. This process our aim to be achieved easier.
5. Question-5
   - The book "Atomic Habits" argues that building new habits is easier if you focus on changing your identity rather than just trying to "change" your behavior. Think of it like this:
     1. **You are who you do**: Your actions are a reflection of the type of person you believe yourself to be, like "a healthy eater" or "a creative writer.
     2. **Change the inner voice**: Instead of saying "I need to exercise more," say "I am the type of person who is active and healthy."
6. Question-6

   - The book "Atomic Habits" by James Clear gives practical tips on how to make habits easier to keep. Here are some important ideas:

     1. **Start small and celebrate small successes:** Instead of trying to change your whole routine at once, begin with small, doable actions. For example, instead of a one-hour workout every day, start with just 10 minutes of walking. Celebrating these small achievements helps create a positive connection with the habit, making it more likely to stick.
     2. **Make it irresistible:** Set up your surroundings and routines to naturally guide you toward the behavior you want. For example, put your workout clothes near your bed so you see them first thing in the morning, or pair a healthy snack with an activity you enjoy.

7. Question-7
   - **The "Never Again" Notebook:** Whenever you give in to the bad habit, write down the situation, trigger, and how you felt afterward. Over time, you'll notice patterns and identify your weak spots. This self-awareness helps you plan ahead and avoid those triggers in the future.
8. Question-8
   - I will the habit of daily workout and meditation, inorder to maintain that I will start with the part like when ever I remember in a day I will follow two rules for some time or for some extinct, this makes us to remember our habit every day, then we can get to it daily.
